<?php

namespace App\Exception;

use Exception;

class ThatsTooMuchException extends Exception
{
    /**
     * @param string $message
     */
    public function __construct(string $message = '')
    {
        $this->message = $message;
    }
}