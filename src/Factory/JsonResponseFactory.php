<?php

namespace App\Factory;

use Symfony\Component\HttpFoundation\JsonResponse;

class JsonResponseFactory
{
    /**
     * @param mixed $data
     * @param int $status
     * @param array $headers
     * @param bool $isJson
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getNewInstance(
        $data,
        int $status = 200,
        array $headers = [],
        bool $isJson = false
    ): JsonResponse {
        return new JsonResponse($data, $status, $headers, $isJson);
    }
}