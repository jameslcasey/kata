<?php

namespace App\Factory;

use Symfony\Component\HttpFoundation\Response;

class HtmlResponseFactory
{
    /**
     * @param null|string $content
     * @param int $status
     * @param array $headers
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getNewInstance(?string $content = '', int $status = 200, array $headers = []): Response
    {
        return new Response($content, $status, $headers);
    }
}