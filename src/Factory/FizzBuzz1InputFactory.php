<?php

namespace App\Factory;

use App\Controller\Input\FizzBuzz1Input;

class FizzBuzz1InputFactory
{
    /**
     * @param int $limit
     *
     * @return \App\Controller\Input\FizzBuzz1Input
     */
    public function getNewInstance(int $limit = 0): FizzBuzz1Input
    {
        return new FizzBuzz1Input($limit);
    }

}