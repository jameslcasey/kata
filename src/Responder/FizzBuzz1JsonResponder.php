<?php

namespace App\Responder;

use App\Factory\JsonResponseFactory;
use Symfony\Component\HttpFoundation\JsonResponse;

class FizzBuzz1JsonResponder
{
    /**
     * @var \App\Factory\JsonResponseFactory
     */
    private $jsonResponseFactory;

    public function __construct(JsonResponseFactory $jsonResponseFactory)
    {
        $this->jsonResponseFactory = $jsonResponseFactory;
    }

    /**
     * @param array $fizzBuzz1Output
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function __invoke(array $fizzBuzz1Output): JsonResponse
    {
        return $this->jsonResponseFactory->getNewInstance(
            json_encode($fizzBuzz1Output),
            200,
            [],
            true
        );
    }
}