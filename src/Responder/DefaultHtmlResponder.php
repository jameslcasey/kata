<?php

namespace App\Responder;

use App\Factory\HtmlResponseFactory;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class DefaultHtmlResponder
{

    /**
     * @var \App\Factory\HtmlResponseFactory
     */
    private $htmlResponseFactory;

    /**
     * @var \Twig\Environment
     */
    private $twig;

    /**
     * @param \Twig\Environment $twig
     * @param \App\Factory\HtmlResponseFactory $htmlResponseFactory
     */
    public function __construct(
        Environment $twig,
        HtmlResponseFactory $htmlResponseFactory)
    {
        $this->htmlResponseFactory = $htmlResponseFactory;
        $this->twig = $twig;
    }

    /**
     * @param null|string $content
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function __invoke(?string $content = ''): Response
    {
        $renderedContent = $this->twig->render('default/default.html.twig', ['content' => $content]);
        return $this->htmlResponseFactory->getNewInstance($renderedContent);
    }

}