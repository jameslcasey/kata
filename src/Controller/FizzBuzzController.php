<?php

namespace App\Controller;

use App\Factory\FizzBuzz1InputFactory;
use App\Responder\FizzBuzz1JsonResponder;
use App\Service\FizzBuzz\FizzBuzz1;
use Symfony\Component\HttpFoundation\JsonResponse;

class FizzBuzzController
{
    /**
     * @var \App\Service\FizzBuzz\FizzBuzz1
     */
    private $fizzBuzz1;

    /**
     * @var \App\Responder\FizzBuzz1JsonResponder
     */
    private $fizzBuzz1JsonResponder;

    /**
     * @var \App\Factory\FizzBuzz1InputFactory
     */
    private $fizzBuzz1InputFactory;

    /**
     * @param \App\Factory\FizzBuzz1InputFactory $fizzBuzz1InputFactory
     * @param \App\Service\FizzBuzz\FizzBuzz1 $fizzBuzz1
     * @param \App\Responder\FizzBuzz1JsonResponder $fizzBuzz1JsonResponder
     */
    public function __construct(
        FizzBuzz1InputFactory $fizzBuzz1InputFactory,
        FizzBuzz1 $fizzBuzz1,
        FizzBuzz1JsonResponder $fizzBuzz1JsonResponder
    ) {
        $this->fizzBuzz1InputFactory = $fizzBuzz1InputFactory;
        $this->fizzBuzz1 = $fizzBuzz1;
        $this->fizzBuzz1JsonResponder = $fizzBuzz1JsonResponder;
    }

    /**
     * @param int $limit
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function __invoke(int $limit = 0): JsonResponse
    {
        $fizzBuzz1Input = $this->fizzBuzz1InputFactory->getNewInstance($limit);
        $fizzBuzz1Output = ($this->fizzBuzz1)($fizzBuzz1Input->getLimit());
        return ($this->fizzBuzz1JsonResponder)($fizzBuzz1Output);
    }
}