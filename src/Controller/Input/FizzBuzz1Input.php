<?php

namespace App\Controller\Input;

use JsonSerializable;

class FizzBuzz1Input implements JsonSerializable
{
    /**
     * @var int
     */
    private $limit;

    /**
     * @param int $limit
     */
    public function __construct(int $limit = 0)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int[]
     */
    public function jsonSerialize(): array
    {
        return [
            'limit' => $this->limit,
        ];
    }
}