<?php

namespace App\Controller;

use App\Responder\DefaultHtmlResponder;
use Symfony\Component\HttpFoundation\Response;

class DefaultController
{

    /**
     * @var \App\Responder\DefaultHtmlResponder
     */
    private $defaultHtmlResponder;

    public function __construct(DefaultHtmlResponder $defaultHtmlResponder)
    {
        $this->defaultHtmlResponder = $defaultHtmlResponder;
    }

    /**
     * @throws \Twig\Error\SyntaxError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\LoaderError
     */
    public function __invoke(): Response
    {
        return ($this->defaultHtmlResponder)('kata things');
    }

}