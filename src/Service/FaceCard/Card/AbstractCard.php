<?php

namespace App\Service\FaceCard\Card;

use LogicException;

abstract class AbstractCard implements CardInterface
{
    public const SUIT_CLUB = 'club';
    public const SUIT_HEART = 'heart';
    public const SUIT_DIAMOND = 'diamond';
    public const SUIT_SPADE = 'spade';
    public const SUIT_JOKER = 'joker';

    public const NAME_ACE = 'ace';
    public const NAME_KING = 'king';
    public const NAME_QUEEN = 'queen';
    public const NAME_JACK = 'jack';
    public const NAME_JOKER = 'joker';

    public const EXCERCISE_HEELS_HEAVENS = 'heels-to-the-heavens';
    public const EXCERCISE_HIGH_KNEES = 'high-knees';
    public const EXCERCISE_MOUNTAIN_CLIMBERS = 'mountain-climbers';
    public const EXCERCISE_JUMP_SQUATS = 'jump-squats';
    public const EXCERCISE_REST = 'rest';

    public const REPS_30 = 30;
    public const REPS_20 = 20;
    public const REPS_15 = 15;
    public const REPS_12 = 12;
    public const REPS_10 = 10;

    /**
     * @var int
     */
    protected $reps;

    /**
     * @var string
     */
    protected $suit;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $exerciseName;

    /**
     * @var bool
     */
    protected $isFaceUp;

    /**
     * @return int
     */
    public function getReps(): int
    {
        throw new LogicException('not implemented');
    }

    /**
     * @return string
     */
    public function getSuit(): string
    {
        throw new LogicException('not implemented');
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        throw new LogicException('not implemented');
    }

    /**
     * @return string
     */
    public function getExerciseName(): string
    {
        throw new LogicException('not implemented');
    }

    /**
     * @return bool
     */
    public function isFaceUp(): bool
    {
        throw new LogicException('not implemented');
    }

}