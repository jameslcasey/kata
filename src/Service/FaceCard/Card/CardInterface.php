<?php

namespace App\Service\FaceCard\Card;

interface CardInterface
{
    public function getReps(): int;

    public function getSuit(): string;

    public function getName(): string;

    public function getExerciseName(): string;

    public function isFaceUp(): bool;
}