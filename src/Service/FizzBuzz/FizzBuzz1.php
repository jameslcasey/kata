<?php

namespace App\Service\FizzBuzz;

use App\Exception\ThatsTooMuchException;

class FizzBuzz1
{
    /**
     *
     */
    public const MAX_LIMIT = 1000;

    /**
     * @param int $limit
     *
     * @return array
     * @throws ThatsTooMuchException
     */
    public function __invoke(int $limit = 0): array
    {
        $result = [];

        if ($limit === 0) {
            return $result;
        }

        if ($limit === 1) {
            $result[] = $limit;
            return $result;
        }

        if ($limit > self::MAX_LIMIT) {
            $limit = self::MAX_LIMIT;
        }

        for ($x = 1; $x <= $limit; $x++) {

            if ($this->isFizzBuzz($x)) {
                $result[] = 'fizzbuzz';
                continue;
            }

            if ($this->isFizz($x)) {
                $result[] = 'fizz';
                continue;
            }

            if ($this->isBuzz($x)) {
                $result[] = 'buzz';
                continue;
            }

            $result[] = $x;
        }

        return $result;
    }

    /**
     * @param int $number
     *
     * @return bool
     */
    private function isFizz(int $number): bool
    {
        return ($number % 3) === 0;
    }

    /**
     * @param int $number
     *
     * @return bool
     */
    private function isBuzz(int $number): bool
    {
        return ($number % 5) === 0;
    }

    /**
     * @param int $number
     *
     * @return bool
     */
    private function isFizzBuzz(int $number): bool
    {
        return $this->isFizz($number) && $this->isBuzz($number);
    }
}