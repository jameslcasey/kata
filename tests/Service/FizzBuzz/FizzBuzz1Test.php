<?php

namespace App\Tests\Service\FizzBuzz;

use App\Exception\ThatsTooMuchException;
use App\Service\FizzBuzz\FizzBuzz1;
use PHPUnit\Framework\TestCase;

class FizzBuzz1Test extends TestCase
{
    /**
     * @var FizzBuzz1
     */
    private $serviceUnderTest;

    protected function setUp(): void
    {
        $this->serviceUnderTest = new FizzBuzz1();
    }

    protected function tearDown(): void
    {
        $this->serviceUnderTest = null;
    }

    public function testV1ReturnsEmptyArray()
    {
        $expected = [];
        $limit = 0;
        $actual = ($this->serviceUnderTest)($limit);
        $this->assertSame($expected, $actual);
    }

    public function testV1ReturnsOne()
    {
        $expected = [1];
        $limit = 1;
        $actual = ($this->serviceUnderTest)($limit);
        $this->assertSame($expected, $actual);
    }

    public function testV1ReturnsLimitThree()
    {
        $expected = [1, 2, 'fizz'];
        $limit = 3;
        $actual = ($this->serviceUnderTest)($limit);
        $this->assertSame($expected, $actual);
    }

    public function testV1ReturnsLimitFive()
    {
        $expected = [1, 2, 'fizz', 4, 'buzz'];
        $limit = 5;
        $actual = ($this->serviceUnderTest)($limit);
        $this->assertSame($expected, $actual);
    }

    public function testV1ReturnsLimitTen()
    {
        $expected = [1, 2, 'fizz', 4, 'buzz', 'fizz', 7, 8, 'fizz', 'buzz'];
        $limit = 10;
        $actual = ($this->serviceUnderTest)($limit);
        $this->assertSame($expected, $actual);
    }

    public function testV1ReturnsLimitFifteen()
    {
        $expected = [1, 2, 'fizz', 4, 'buzz', 'fizz', 7, 8, 'fizz', 'buzz', 11, 'fizz', 13, 14, 'fizzbuzz'];
        $limit = 15;
        $actual = ($this->serviceUnderTest)($limit);
        $this->assertSame($expected, $actual);
    }

    public function testV1ReturnsLimitOneHundred()
    {
        $expected = [
            0 => 1,
            1 => 2,
            2 => 'fizz',
            3 => 4,
            4 => 'buzz',
            5 => 'fizz',
            6 => 7,
            7 => 8,
            8 => 'fizz',
            9 => 'buzz',
            10 => 11,
            11 => 'fizz',
            12 => 13,
            13 => 14,
            14 => 'fizzbuzz',
            15 => 16,
            16 => 17,
            17 => 'fizz',
            18 => 19,
            19 => 'buzz',
            20 => 'fizz',
            21 => 22,
            22 => 23,
            23 => 'fizz',
            24 => 'buzz',
            25 => 26,
            26 => 'fizz',
            27 => 28,
            28 => 29,
            29 => 'fizzbuzz',
            30 => 31,
            31 => 32,
            32 => 'fizz',
            33 => 34,
            34 => 'buzz',
            35 => 'fizz',
            36 => 37,
            37 => 38,
            38 => 'fizz',
            39 => 'buzz',
            40 => 41,
            41 => 'fizz',
            42 => 43,
            43 => 44,
            44 => 'fizzbuzz',
            45 => 46,
            46 => 47,
            47 => 'fizz',
            48 => 49,
            49 => 'buzz',
            50 => 'fizz',
            51 => 52,
            52 => 53,
            53 => 'fizz',
            54 => 'buzz',
            55 => 56,
            56 => 'fizz',
            57 => 58,
            58 => 59,
            59 => 'fizzbuzz',
            60 => 61,
            61 => 62,
            62 => 'fizz',
            63 => 64,
            64 => 'buzz',
            65 => 'fizz',
            66 => 67,
            67 => 68,
            68 => 'fizz',
            69 => 'buzz',
            70 => 71,
            71 => 'fizz',
            72 => 73,
            73 => 74,
            74 => 'fizzbuzz',
            75 => 76,
            76 => 77,
            77 => 'fizz',
            78 => 79,
            79 => 'buzz',
            80 => 'fizz',
            81 => 82,
            82 => 83,
            83 => 'fizz',
            84 => 'buzz',
            85 => 86,
            86 => 'fizz',
            87 => 88,
            88 => 89,
            89 => 'fizzbuzz',
            90 => 91,
            91 => 92,
            92 => 'fizz',
            93 => 94,
            94 => 'buzz',
            95 => 'fizz',
            96 => 97,
            97 => 98,
            98 => 'fizz',
            99 => 'buzz',

        ];
        $limit = 100;
        $actual = ($this->serviceUnderTest)($limit);
        $this->assertSame($expected, $actual);
    }

    public function testV1ReturnsMaxLimit()
    {
        $actual = ($this->serviceUnderTest)(1001);
        $this->assertCount(
            FizzBuzz1::MAX_LIMIT,
            $actual,
            'Should return count of max limit.'
        );
    }
}
